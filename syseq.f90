! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
Module system_of_equations
implicit none
contains
function system_eq(time,vals)
    USE RKSUITE_90_PREC
    use parameters
    implicit none
    REAL(WP), INTENT(IN) :: time
    REAL(WP), DIMENSION(:), INTENT(IN) :: vals
    REAL(KIND=WP), DIMENSION(SIZE(vals)) :: system_eq
    REAL(WP),DIMENSION(num_base_eq,people_categories,states)::SEIR,dSEIRdt,THIS_DUMMY_VECTOR
    LOGICAL,DIMENSION(num_base_eq,people_categories,states)::THIS_MASK    
    REAL(WP),DIMENSION(people_categories,states)::This_S,This_E,This_IASYMP,This_ISYMP
    REAL(WP),DIMENSION(people_categories,states)::This_IMILD,This_ISVR,This_IHPTL
    REAL(WP),DIMENSION(people_categories,states)::This_DEAD,This_REC,This_N
    INTEGER::j,k,INT_DAY

 
    INT_DAY=INT(time) ! Used to identify whether to impose Lockdown or not
    ! for variable Lockdowns in different states, changing in system_of_equations
    ! is better than integrate routine
 
    THIS_MASK=.True.
    THIS_DUMMY_VECTOR=0d0
    SEIR=UNPACK(vals,THIS_MASK,THIS_DUMMY_VECTOR)
    
    This_S(:,:)=SEIR(susceptible,:,:)
    This_E(:,:)=SEIR(exposed,:,:)
    This_IASYMP(:,:)=SEIR(asymptomatic,:,:)
    This_ISYMP(:,:)=SEIR(symptomatic,:,:)
    This_IMILD(:,:)=SEIR(mild,:,:)
    This_ISVR(:,:)=SEIR(severe,:,:)
    This_IHPTL(:,:)=SEIR(Hospitalized,:,:)
    This_DEAD(:,:)=SEIR(dead,:,:)
    This_REC(:,:)=SEIR(recovered,:,:)
    
    eff_asymp=Initial_eff_asymp*exp(-time/tau_asymp)
    eff_symp=Initial_eff_symp*exp(-time/tau_symp)
    eff_mild=Initial_eff_mild*exp(-time/tau_mild)
    eff_svr=Initial_eff_svr*exp(-time/tau_svr)
    !Infectivity decreases with time due to tests and quarantining
    
    forall(j=1:people_categories,k=1:states) This_N(j,k)=sum(SEIR(:,j,k))

     !    print*,'Total Infected:',This_infected,'Today:',INT_DAY
    ! 9 equations set
    do k=1,states    
        do j=1,people_categories        
        dSEIRdt(susceptible,j,k)=-This_S(j,k)*sum(beta_matrix(j,:,k)*contact(j,:,k)*(eff_asymp(k)*This_IASYMP(:,k)&
                        &+eff_symp(k)*This_ISYMP(:,k)+eff_mild(k)*This_IMILD(:,k)+eff_svr(k)*This_ISVR(:,k))/This_N(:,k))&
                        &+sum(mobility_matrix(j,k,:)*This_S(j,:))&
                        &-This_S(j,k)*sum(mobility_matrix(j,:,k))
                        
        dSEIRdt(exposed,j,k)=This_S(j,k)*sum(beta_matrix(j,:,k)*contact(j,:,k)*(eff_asymp(k)*This_IASYMP(:,k)&
                        &+eff_symp(k)*This_ISYMP(:,k)+eff_mild(k)*This_IMILD(:,k)+eff_svr(k)*This_ISVR(:,k))/This_N(:,k))&
                        &-(C_E2IASY(j,k)+C_E2ISY(j,k))*This_E(j,k)&
                        &+sum(mobility_matrix(j,k,:)*This_E(j,:))&
                        &-This_E(j,k)*sum(mobility_matrix(j,:,k))
                        
        dSEIRdt(asymptomatic,j,k)=C_E2IASY(j,k)*This_E(j,k)-C_IASY2R(j,k)*This_IASYMP(j,k)&
                        &+sum(mobility_matrix(j,k,:)*This_IASYMP(j,:))&
                        &-This_IASYMP(j,k)*sum(mobility_matrix(j,:,k))

        dSEIRdt(symptomatic,j,k)=C_E2ISY(j,k)*This_E(j,k)-(C_ISY2MLD(j,k)+C_ISY2SVR(j,k))*This_ISYMP(j,k)&
                        &+sum(mobility_matrix(j,k,:)*This_ISYMP(j,:))&
                        &-This_ISYMP(j,k)*sum(mobility_matrix(j,:,k))

        dSEIRdt(mild,j,k)=C_ISY2MLD(j,k)*This_ISYMP(j,k)-C_MLD2R(j,k)*This_IMILD(j,k)&
                        &+sum(mobility_matrix(j,k,:)*This_IMILD(j,:))&
                        &-This_IMILD(j,k)*sum(mobility_matrix(j,:,k))

        dSEIRdt(severe,j,k)=C_ISY2SVR(j,k)*This_ISYMP(j,k)-(C_SVR2HPTL(j,k)+C_SVR2R(j,k)+C_SVR2D(j,k))*This_ISVR(j,k)
        
        !+sum(mobility_matrix(j,k,:)*This_IMILD(j,:))-This_IMILD(j,k)*sum(mobility_matrix(j,:,k))
        ! NO MOBILITY FOR SEVERE CASES                

        dSEIRdt(Hospitalized,j,k)=C_SVR2HPTL(j,k)*This_ISVR(j,k)-(C_HPTL2R(j,k)+C_HPTL2D(j,k))*This_IHPTL(j,k)

        dSEIRdt(dead,j,k)=C_HPTL2D(j,k)*This_IHPTL(j,k)+C_SVR2D(j,k)*This_ISVR(j,k)
                
        
        dSEIRdt(recovered,j,k)=C_HPTL2R(j,k)*This_IHPTL(j,k)+C_SVR2R(j,k)*This_ISVR(j,k)&
                        &+C_MLD2R(j,k)*This_IMILD(j,k)+C_IASY2R(j,k)*This_IASYMP(j,k)&
                        &+sum(mobility_matrix(j,k,:)*This_REC(j,:))&
                        &-This_REC(j,k)*sum(mobility_matrix(j,:,k))
        end do
    end do

    system_eq=pack(dSEIRdt,.True.)
end function system_eq
end Module system_of_equations
