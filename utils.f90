! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
! haversine code used from https://rosettacode.org/wiki/Haversine_formula
module utils
  USE RKSUITE_90_PREC
  implicit none
  
contains

  function to_radian(degree) result(rad)
    ! degrees to radians
    Double precision,intent(in) :: degree
    Double precision, parameter :: deg_to_rad = atan(1.0)/45 ! exploit intrinsic atan to generate pi/180 runtime constant
    Double precision :: rad

    rad = degree*deg_to_rad
  end function to_radian

  function haversine(deglat1,deglon1,deglat2,deglon2) result (dist)
    ! great circle distance -- adapted from Matlab 
    Double precision,intent(in) :: deglat1,deglon1,deglat2,deglon2
    Double precision :: a,c,dist,dlat,dlon,lat1,lat2
    Double precision,parameter :: radius = 6372.8 

    dlat = to_radian(deglat2-deglat1)
    dlon = to_radian(deglon2-deglon1)
    lat1 = to_radian(deglat1)
    lat2 = to_radian(deglat2)
    a = (sin(dlat/2))**2 + cos(lat1)*cos(lat2)*(sin(dlon/2))**2
    c = 2*asin(sqrt(a))
    dist = radius*c
  end function haversine


  subroutine branching(these_categories_to_divide,these_num_branches)
    use parameters
    use inifile
    integer,intent(in)::these_num_branches
    logical,dimension(:),intent(in)::these_categories_to_divide
    integer::i,j,k,this_categorynum,total_branch
    character(LEN=Ini_max_string_len)::this_categoryname
    character(LEN=Ini_max_string_len),allocatable,dimension(:)::main_categorynames,temp_name_array
    real(wp),allocatable,dimension(:,:)::main_populationfraction


    main_categories=people_categories    
    main_categorynames=category_name
    main_populationfraction=Population_fraction
    print*,'==========================================================='
    print*,'Branching: this will change the number of people_categories'
    print*,'Branching: this will redistribute the population fraction'
    print*,'Branching: this will rename the category names'
    print*,'==========================================================='
    this_categorynum=0
    deallocate(category_name,Population_fraction)
    allocate(category_name(0))


    do i=1,main_categories
       total_branch=1
       if(these_categories_to_divide(i)) total_branch=these_num_branches 
       do j=1,total_branch
          this_categorynum=this_categorynum+1
          allocate(temp_name_array(this_categorynum))

          if(this_categorynum>1) temp_name_array(1:this_categorynum-1)=category_name(1:this_categorynum-1)

          temp_name_array(this_categorynum)=main_categorynames(i)
          write(this_categoryname,'(i10)') j
          if(total_branch>1) temp_name_array(this_categorynum)=&
               &trim(main_categorynames(i))//trim('-Group-')//trim(adjustl(this_categoryname))
          call move_alloc(temp_name_array,category_name)

       end do
    end do
    !     print*,'came here'

    people_categories=this_categorynum


    !     print*,main_populationfraction
    allocate(Population_fraction(people_categories,states))
    if(allocated(branch_belongs_to)) deallocate(branch_belongs_to)
    allocate(branch_belongs_to(people_categories),scale_IC(people_categories),branch_begin(main_categories))

    do k=1,states
       this_categorynum=0
       do i=1,main_categories 
          total_branch=1
          if(these_categories_to_divide(i)) total_branch=these_num_branches
          branch_begin(i)=this_categorynum+1
          do j=1,total_branch
             this_categorynum=this_categorynum+1
             Population_fraction(this_categorynum,k)=main_populationfraction(i,k)/total_branch
             if(k==1) branch_belongs_to(this_categorynum)=i ! just fix it once
             if(k==1) scale_IC(this_categorynum)=1d0/dble(total_branch)                
             !                   print*,k,i,j,branch_belongs_to(this_categorynum),&
             !                   &Population_fraction(this_categorynum,k)
             !                  pause
          end do
       end do
    end do
    print*,branch_begin
    pause
  end subroutine branching

  
  subroutine get_contact_matrix(is_uniform,base_contact_filename,base_pop_frac,subgroups,write_contact_file)
    use parameters
    use inifile
    implicit none
    integer::i,j,k,filenum,num_place_for_contacts
    real(wp)::row_sum
    logical,intent(in)::is_uniform
    integer,dimension(:),optional,intent(in)::subgroups 
    double precision,dimension(:,:),optional,intent(in)::base_pop_frac
    character(len=*),optional,intent(in),dimension(:)::base_contact_filename,write_contact_file
    character(len=Ini_max_string_len)::filename 
    !======= for uniform contact ===========================
    if(is_uniform) then     
        contact=1d0
    else        
        if((.not.present(base_contact_filename)).or.&
        &(.not.present(base_pop_frac)).or.(.not.present(subgroups))) stop 'Base matrix needed for non-uniform contacts' 
        num_place_for_contacts=size(base_contact_filename)
        
        if(.not.(allocated(Variational_contact))) &
        &allocate(Variational_contact(people_categories,people_categories,states,num_contact_places))

        do k=1,states
            do i=1,num_place_for_contacts
            call make_contact(base_contact_filename(i),base_pop_frac(k,:),subgroups,Variational_contact(:,:,k,i))        
            end do
        end do
        
    end if
    
    if(present(write_contact_file)) then 
    do k=1,states  
       do j=1,num_place_for_contacts
     
!         pause 'came here'
       filename=trim(write_contact_file(j))//trim(adjustl("-"))//trim(adjustl(selected_states(k)))//".txt"      
!        print*,filename
       OPEN(NEWUNIT=filenum,FILE=filename,action='write',status='REPLACE')   
       do i=1,size(contact,1)    
            write(filenum,*)Variational_contact(i,:,k,j) 
       end do
       close(filenum)
      end do 
    end do   
    end if 
    
   if(is_uniform) then     
    do k=1,states
       do i=1,people_categories 
        row_sum=0d0
        contact(i,:,k)=Population_fraction(:,k)*contact(i,:,k)        
       end do
    end do
    end if
    
  end subroutine get_contact_matrix
  
subroutine make_contact(base_contact_filename,base_pop_frac,subgroups_for_reduced_groups,this_reduced_contact)
    use parameters
    implicit none
    double precision,dimension(:),intent(in)::base_pop_frac
    double precision,dimension(:,:),intent(out)::this_reduced_contact
    double precision,dimension(:,:),allocatable::base_contact_matrix
    double precision::k_sum
    character(len=*),intent(in)::base_contact_filename
    integer,dimension(:),intent(in)::subgroups_for_reduced_groups
    integer,dimension(:),allocatable::start_group,end_group,subgroups    
    integer::i,j,k,num_reduced_frac,contact_filenum,num_base
    
    num_base=size(base_pop_frac)    
    num_reduced_frac=size(this_reduced_contact,1)
    allocate(base_contact_matrix(num_base,num_base))
    allocate(start_group(num_reduced_frac),end_group(num_reduced_frac))

    start_group(1)=1
    do i=1,num_reduced_frac
        if(i>1) start_group(i)=end_group(i-1)+1
        end_group(i)=start_group(i)+subgroups_for_reduced_groups(i)-1
    end do
    if(end_group(num_reduced_frac)/=num_base) stop 'Age brackets are not distributed properly for reduced groups'
    
    OPEN(NEWUNIT=contact_filenum,FILE=base_contact_filename,action='read',status='old')  
        do i=1,num_base
            read(contact_filenum,*)base_contact_matrix(:,i)
        end do
     close(contact_filenum)
    
    ! Diagonal terms 
    do i=1,num_reduced_frac
        this_reduced_contact(i,i)=0d0
        do j=start_group(i),end_group(i)
        this_reduced_contact(i,i)=this_reduced_contact(i,i)+base_pop_frac(j)*sum(base_contact_matrix(start_group(i):end_group(i),j))
        end do
        this_reduced_contact(i,i)=this_reduced_contact(i,i)/sum(base_pop_frac(start_group(i):end_group(i)))        
    end do
    
    do i=1,num_reduced_frac
        do j=1,num_reduced_frac
        if(i/=j) then
            this_reduced_contact(j,i)=0d0
            k_sum=0d0
            do k=start_group(i),end_group(i)
                k_sum=sum(base_contact_matrix(k,start_group(j):end_group(j))*base_pop_frac(start_group(j):end_group(j)))
                k_sum=k_sum/sum(base_pop_frac(start_group(j):end_group(j)))
                this_reduced_contact(j,i)=this_reduced_contact(j,i)+k_sum
            end do
        end if
        end do
    end do    
    
end subroutine make_contact  
  ! subroutine prepare_lockdown(this_state_num,Lockdown_Type,param1,param2,param3,startday_array,duration_array)
  !     implicit none
  !     use parameters
  !     use inifile
  !     character(Len=*),intent(in)::Lockdown_Type
  !     integer,intent(in)::param1,param2,this_state_num
  !     integer,intent(in),optional::param3
  !     integer,intent(in),dimension(:),optional::startday_array,duration_array
  !     
  ! 
  !     ! For Periodic Synchronous
  !     ! param1=
  !     ! param2=switch off threshold
  !     
  !     
  !     if(Lockdown_Type=='LightSwitch_Infected')
  !     
  !     
  !     
  !     
  !     
  !     
  !     
  !     ! for simple lockdown
  !     ! param1=start_day/none if multiple lockdown used
  !     ! param2=duration/none if multiple lockdown used
  !     ! param3=not used
  !     ! startday_array = for multiple lockdowns
  !     ! duration_array = for multiple lockdowns
  !     
  !     
  ! end subroutine prepare_lockdown


  subroutine makeplots(filestoplot_DIM2,filestoplot_DIM1,statenames,categories)
    use parameters, only: implement_lockdown, Lockdown,steps_days,Is_Uniform_contact,Mobility
    implicit none
    character(len=*),dimension(:,:),intent(in)::filestoplot_DIM2
    character(len=*),dimension(:),intent(in)::filestoplot_DIM1
    character(len=*),dimension(:),intent(in)::statenames,categories    
    character(len=128)::plotscript_file,tic_name,write_contact_name
    integer::GNUPLOT_UNIT,i,j,day,k
    integer,allocatable,dimension(:)::band_start,band_end,temp_array


    plotscript_file='plots.p'
    OPEN(NEWUNIT=GNUPLOT_UNIT,FILE=plotscript_file,FORM='FORMATTED',STATUS='REPLACE')
    write(GNUPLOT_UNIT,*)'se te pdf enhanced color font "Times Italic,12"'
    write(GNUPLOT_UNIT,*)'set tics scale 0'    
    write(GNUPLOT_UNIT,*)'set ytics format "%g"'
    write(GNUPLOT_UNIT,*)'set xtics format "%g"'
    write(GNUPLOT_UNIT,*)'set border lw 3'
    write(GNUPLOT_UNIT,*)'set grid'
    write(GNUPLOT_UNIT,*)'unset border'
    write(GNUPLOT_UNIT,*)'set label "INDSCI-SIM" at 2,graph 0.8 textcolor rgb "#6495ED" font "Helvetica,12"'

    write(GNUPLOT_UNIT,*)'set output "ELIXSIR_state.pdf" '    
    write(GNUPLOT_UNIT,*)'set xlabel "t in days" font "Times Italic,12" '
    write(GNUPLOT_UNIT,*)'set ylabel "Population in different conditions (t)" font "Times Italic,12"'
    write(GNUPLOT_UNIT,*)'set xtics nomirror font "Times Italic,12" '
    write(GNUPLOT_UNIT,*)'set ytics nomirror font "Times Italic,12"'    
    write(GNUPLOT_UNIT,*)'set xrange [1:]'
    write(GNUPLOT_UNIT,*)'set yrange [1:]'    
    write(GNUPLOT_UNIT,*)'unset key'
    write(GNUPLOT_UNIT,*)'set title font "Times Italic,8"'
    write(GNUPLOT_UNIT,*)'set key top right Right horizontal autotitle columnheader samplen -1 tc variable spacing 1'

    do j=1,size(filestoplot_DIM2,2)          
       do i=1,size(filestoplot_DIM2,1)
          write(GNUPLOT_UNIT,*)'unset object'
          
          write(GNUPLOT_UNIT,*)'set grid ls 12 lw 1.5 lc rgb "white"'

          write(GNUPLOT_UNIT,*)'set style rect fc lt -1 fs solid 0.05 noborder'

          write(GNUPLOT_UNIT,*)'set obj rect from graph 0, 0 to graph 1, 1 behind'
       
          lockdown_check:if(implement_lockdown) then 

             allocate(band_start(0),band_end(0))

             if(Lockdown(j,i,1)) then
                deallocate(band_start)
                allocate(band_start(1))
                band_start(1)=1
             end if

             do day=2,steps_days        
                if(Lockdown(j,i,day-1).and.(.not.Lockdown(j,i,day))) then
                   allocate(temp_array(size(band_end)+1))
                   temp_array(1:size(band_end))=band_end(1:size(band_end))
                   temp_array(size(temp_array))=day
                   call move_alloc(temp_array,band_end)    
                elseif(Lockdown(j,i,day).and.(.not.Lockdown(j,i,day-1))) then
                   allocate(temp_array(size(band_start)+1))
                   if(size(band_start)/=0) temp_array(1:size(band_start))=band_start(1:size(band_start))            
                   temp_array(size(temp_array))=day
                   call move_alloc(temp_array,band_start)                
                end if
             end do


             if(size(band_start)>size(band_end)) then
                ! It did not find the end of final lockdown within final date of simulation
                ! Keep the dimensions same and keep the end date as the band end
                ! 
                allocate(temp_array(size(band_end)+1))
                temp_array(1:size(band_end))=band_end(1:size(band_end))            
                temp_array(size(temp_array))=steps_days
                call move_alloc(temp_array,band_end)       
                if(size(band_start)>size(band_end)) stop 'Lockdown band sizes do not match'
             end if


             print*,band_start
             print*,band_end
             !pause
             
             write(GNUPLOT_UNIT,*)'unset logscale'
!              write(GNUPLOT_UNIT,*)'unset object'
             write(GNUPLOT_UNIT,*) 'set style rect fc rgb "blue" fs solid 0.15 noborder'

             do k=1,size(band_start)
                write(GNUPLOT_UNIT,*)  'set obj rect from',band_start(k),', graph 0 to',band_end(k),', graph 1 behind'
             end do
             deallocate(band_start,band_end)
          end if lockdown_check


          write(GNUPLOT_UNIT,*)'set title "',trim(adjustl(statenames(j)))&
               &,', population category: ',trim(adjustl(categories(i))),'"'
          !         write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot_DIM2(i,j)),'" ','w l lw 3,',&
          !         &' ""  u 1:3 w l lw 3,',' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
          !         &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
          !         &' ""  u 1:9 w l lw 3,',' "" u 1:10 w l lc rgb "grey" lw 3'        
          write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot_DIM2(i,j)),'" ',' u 1:3 w l lw 3,'&
               &,' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
               &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
               &' ""  u 1:9 w l lw 3'     
          write(GNUPLOT_UNIT,*)'set log y'

          write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot_DIM2(i,j)),'" ',' u 1:3 w l lw 3,'&
               &,' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
               &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
               &' ""  u 1:9 w l lw 3'        

       end do

    end do

    write(GNUPLOT_UNIT,*)'set output "ELIXSIR_New_and_Cumulative_Infected.pdf" '    

    do j=1,size(filestoplot_DIM1)
       write(GNUPLOT_UNIT,*)'unset border'    
       write(GNUPLOT_UNIT,*)'unset object'
       write(GNUPLOT_UNIT,*)'set style rect fc lt -1 fs solid 0.05 noborder'
       write(GNUPLOT_UNIT,*)'set obj rect from graph 0, 0 to graph 1,  1 behind'       
       write(GNUPLOT_UNIT,*)'set multiplot'
       write(GNUPLOT_UNIT,*)'set size 1,1'        
       write(GNUPLOT_UNIT,*)'set origin 0,0'                
       write(GNUPLOT_UNIT,*)'unset logscale'
       write(GNUPLOT_UNIT,*)'unset xrange'
       write(GNUPLOT_UNIT,*)'unset label'
       write(GNUPLOT_UNIT,*)'set label "INDSCI-SIM" at 2,graph 0.8 textcolor rgb "#6495ED" font "Helvetica,12"'
       write(GNUPLOT_UNIT,*)'unset yrange'
       write(GNUPLOT_UNIT,*)'unset xlabel'
       write(GNUPLOT_UNIT,*)'unset ylabel'        
       write(GNUPLOT_UNIT,*)'set xtics nomirror font "Times Italic,12" '
       write(GNUPLOT_UNIT,*)'set ytics nomirror font "Times Italic,12"'            
       write(GNUPLOT_UNIT,*)'set ylabel "Infected/Dead population" '
       write(GNUPLOT_UNIT,*)'set xlabel "Days"'                
       write(GNUPLOT_UNIT,*)'set xrange [1:]'
       write(GNUPLOT_UNIT,*)'set yrange [1:]'    
       write(GNUPLOT_UNIT,*)'unset key'
       write(GNUPLOT_UNIT,*)'set title font "Times Italic,8"'
       write(GNUPLOT_UNIT,*)'set key top left Left vertical autotitle columnheader samplen -1 tc variable spacing 1'

       write(GNUPLOT_UNIT,*)'set title "State: ',trim(adjustl(statenames(j))),'"'

       write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot_DIM1(j)),'" ',' u 1:2 w l lw 3,'&
            &,' "" u 1:3 w l lw 3,',' "" u 1:4 w l lw 3 lc rgb "red"'        
       write(GNUPLOT_UNIT,*)'set size 0.58,0.7'   
       write(GNUPLOT_UNIT,*)'unset title'
       write(GNUPLOT_UNIT,*)'unset label'
       write(GNUPLOT_UNIT,*)'set border lw 2'           
       write(GNUPLOT_UNIT,*)'set ylabel font "Times Italic,8" offset 2'
       write(GNUPLOT_UNIT,*)'set xlabel font "Times Italic,8" offset 0,2'                        
       write(GNUPLOT_UNIT,*)'set ytics  font "Times Italic,8" '
       write(GNUPLOT_UNIT,*)'set xtics  font "Times Italic,8" '                                
       write(GNUPLOT_UNIT,*)'set origin 0.4,0.15'                
       write(GNUPLOT_UNIT,*)'set xrange [20:150]'        
       write(GNUPLOT_UNIT,*)'set yrange [1:]'    
       write(GNUPLOT_UNIT,*)'unset key'
       write(GNUPLOT_UNIT,*) 'set log y'
       write(GNUPLOT_UNIT,*) 'replot'

    end do
    close(GNUPLOT_UNIT)

    write(*,*) "PlotScript Created"        
    write(*,*) "Now run gnuplot > load 'plots.p'"        
    
    if(Mobility) then
        plotscript_file='Mobilityplots.p'
        OPEN(NEWUNIT=GNUPLOT_UNIT,FILE=plotscript_file,FORM='FORMATTED',STATUS='REPLACE')
        write(GNUPLOT_UNIT,*)'se te pdf enhanced color font "Times Italic,12"'
        write(GNUPLOT_UNIT,*)'set tics scale 0'    
        write(GNUPLOT_UNIT,*)'set ytics format "%g"'
        write(GNUPLOT_UNIT,*)'set xtics format "%g"'
        write(GNUPLOT_UNIT,*)'set border lw 3'
        write(GNUPLOT_UNIT,*)'set size square'
!         write(GNUPLOT_UNIT,*)'unset border'
        write(GNUPLOT_UNIT,*)' set label "INDSCI-SIM" at graph 0.7,0.05 front textcolor rgb "#6495ED" font "Helvetica,10"'

        write(GNUPLOT_UNIT,*)'set output "ELIXSIR_Mobility.pdf" '    
        write(GNUPLOT_UNIT,*)'set xlabel " " font "Times Italic,12" '
        write(GNUPLOT_UNIT,*)'set ylabel " " font "Times Italic,12"'
        write(GNUPLOT_UNIT,*)'set xtics nomirror font "Times Italic,12" '
        write(GNUPLOT_UNIT,*)'set ytics nomirror font "Times Italic,12"'    
        write(GNUPLOT_UNIT,*)'unset key'
        write(GNUPLOT_UNIT,*)'set cblabel "Traffic"'
!         write(GNUPLOT_UNIT,*) 'load "color-template/viridis.pal"'
        write(GNUPLOT_UNIT,*) 'set xtics \'    
        do i=0,size(filestoplot_DIM2,2)-1
        
        write(tic_name,'(i10)') i 
        if(i==0) then 
            write(GNUPLOT_UNIT,*) '("',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),',\'
        elseif(i==size(filestoplot_DIM2,2)-1) then 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),')\'
        else 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),',\'        
        end if
        end do
        write(GNUPLOT_UNIT,*) 'rotate by 90 offset 0,-2.5 font "Times Italic,6"'   
        write(GNUPLOT_UNIT,*) 'set ytics \'    
        do i=0,size(filestoplot_DIM2,2)-1
        
        write(tic_name,'(i10)') i 
        if(i==0) then 
            write(GNUPLOT_UNIT,*) '("',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),',\'
        elseif(i==size(filestoplot_DIM2,2)-1) then 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),')\'
        else 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(statenames(i+1))),'"',trim(adjustl(tic_name)),',\'        
        end if
        end do
        write(GNUPLOT_UNIT,*) 'font "Times Italic,6"'   
        write(GNUPLOT_UNIT,*) 'set title "Migration gravity algorithm, the force"'           
        write(GNUPLOT_UNIT,*)'pl "stored_data/Migration_matrix/Migration.txt" matrix w image'
        close(GNUPLOT_UNIT)
        write(*,*) "Now run gnuplot > load 'Mobilityplots.p' to generate display Mobility matrix"        
        
    end if

    
    
    if(.not.(Is_Uniform_contact)) then 
        plotscript_file='Contacts.p'
        OPEN(NEWUNIT=GNUPLOT_UNIT,FILE=plotscript_file,FORM='FORMATTED',STATUS='REPLACE')
        write(GNUPLOT_UNIT,*)'se te pdf enhanced color font "Times Italic,12"'
        write(GNUPLOT_UNIT,*)'set tics scale 0'    
        write(GNUPLOT_UNIT,*)'set ytics format "%g"'
        write(GNUPLOT_UNIT,*)'set xtics format "%g"'
        write(GNUPLOT_UNIT,*)'set border lw 3'
        write(GNUPLOT_UNIT,*)'set size square'
!         write(GNUPLOT_UNIT,*)'unset border'
        write(GNUPLOT_UNIT,*)' set label "INDSCI-SIM" at graph 0.7,0.05 front textcolor rgb "#6495ED" font "Helvetica,10"'

        write(GNUPLOT_UNIT,*)'set output "ELIXSIR_Contacts.pdf" '    
        write(GNUPLOT_UNIT,*)'set xlabel " " font "Times Italic,12" '
        write(GNUPLOT_UNIT,*)'set ylabel " " font "Times Italic,12"'
        write(GNUPLOT_UNIT,*)'set xtics nomirror font "Times Italic,12" '
        write(GNUPLOT_UNIT,*)'set ytics nomirror font "Times Italic,12"'    
        write(GNUPLOT_UNIT,*)'unset key'
        write(GNUPLOT_UNIT,*)'set cblabel "Contacts"'
        write(GNUPLOT_UNIT,*) 'load "color-template/viridis.pal"'
        write(GNUPLOT_UNIT,*) 'set xtics \'    
        do i=0,size(filestoplot_DIM2,1)-1
        
        write(tic_name,'(i10)') i 
        if(i==0) then 
            write(GNUPLOT_UNIT,*) '("',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),',\'
        elseif(i==size(filestoplot_DIM2,1)-1) then 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),')\'
        else 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),',\'        
        end if
        end do
        write(GNUPLOT_UNIT,*) 'rotate by 90 offset 0,-2.5 font "Times Italic,8"'   
        write(GNUPLOT_UNIT,*) 'set ytics \'   
        do i=0,size(filestoplot_DIM2,1)-1
        write(tic_name,'(i10)') i 
        if(i==0) then 
            write(GNUPLOT_UNIT,*) '("',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),',\'
        elseif(i==size(filestoplot_DIM2,1)-1) then 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),')\'
        else 
            write(GNUPLOT_UNIT,*) '"',trim(adjustl(categories(i+1))),'"',trim(adjustl(tic_name)),',\'        
        end if
        end do
        write(GNUPLOT_UNIT,*) 'font "Times Italic,8"'   
        
        
        do i=1,size(filestoplot_DIM2,2)
        write(GNUPLOT_UNIT,*) 'set title "Region: ',trim(adjustl(statenames(i))),': Contacts during lockdown"'
            
        write_contact_name='stored_data/contact-store'//"/"//trim("ELIXSIR_Contact_Lockdown_")//&
             &trim(adjustl(statenames(i)))//".txt"
        
        write(GNUPLOT_UNIT,*)'pl "',trim(adjustl(write_contact_name)),'" matrix w image'

        write_contact_name='stored_data/contact-store'//"/"//trim("ELIXSIR_Contact_Unlock_")//&
             &trim(adjustl(statenames(i)))//".txt"
        

        write(GNUPLOT_UNIT,*) 'set title "Region: ',trim(adjustl(statenames(i))),': Contacts during Unlock"'           
        write(GNUPLOT_UNIT,*)'pl "',trim(adjustl(write_contact_name)),'" matrix w image'
        
        end do
        
        close(GNUPLOT_UNIT)
        write(*,*) "Now run gnuplot > load 'Contacts.p' to generate display contact matrix"        
        
    
    end if
    

  end subroutine makeplots
end module utils
