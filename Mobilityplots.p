 se te pdf enhanced color font "Times Italic,12"
 set tics scale 0
 set ytics format "%g"
 set xtics format "%g"
 set border lw 3
 set size square
  set label "INDSCI-SIM" at graph 0.7,0.05 front textcolor rgb "#6495ED" font "Helvetica,10"
 set output "ELIXSIR_Mobility.pdf" 
 set xlabel " " font "Times Italic,12" 
 set ylabel " " font "Times Italic,12"
 set xtics nomirror font "Times Italic,12" 
 set ytics nomirror font "Times Italic,12"
 unset key
 set cblabel "Traffic"
 set xtics \
 ("State:TAMILNADU : District:Thiruvallur (Urban)"0,\
 "State:TAMILNADU : District:Chennai (Urban)"1,\
 "State:TAMILNADU : District:Kancheepuram (Urban)"2,\
 "State:TAMILNADU : District:Vellore (Rural)"3,\
 "State:TAMILNADU : District:Tiruvannamalai (Rural)"4,\
 "State:TAMILNADU : District:Viluppuram (Rural)"5,\
 "State:TAMILNADU : District:Salem (Urban)"6,\
 "State:TAMILNADU : District:Namakkal (Rural)"7,\
 "State:TAMILNADU : District:Erode (Urban)"8,\
 "State:TAMILNADU : District:The-Nilgiris (Urban)"9,\
 "State:TAMILNADU : District:Dindigul (Rural)"10,\
 "State:TAMILNADU : District:Karur (Rural)"11,\
 "State:TAMILNADU : District:Tiruchirappalli (Rural)"12,\
 "State:TAMILNADU : District:Perambalur (Rural)"13,\
 "State:TAMILNADU : District:Ariyalur (Rural)"14,\
 "State:TAMILNADU : District:Cuddalore (Rural)"15,\
 "State:TAMILNADU : District:Nagapattinam (Rural)"16,\
 "State:TAMILNADU : District:Thiruvarur (Rural)"17,\
 "State:TAMILNADU : District:Thanjavur (Rural)"18,\
 "State:TAMILNADU : District:Pudukkottai (Rural)"19,\
 "State:TAMILNADU : District:Sivaganga (Rural)"20,\
 "State:TAMILNADU : District:Madurai (Urban)"21,\
 "State:TAMILNADU : District:Theni (Urban)"22,\
 "State:TAMILNADU : District:Virudhunagar (Urban)"23,\
 "State:TAMILNADU : District:Ramanathapuram (Rural)"24,\
 "State:TAMILNADU : District:Thoothukkudi (Urban)"25,\
 "State:TAMILNADU : District:Tirunelveli (Rural)"26,\
 "State:TAMILNADU : District:Kanniyakumari (Urban)"27,\
 "State:TAMILNADU : District:Dharmapuri (Rural)"28,\
 "State:TAMILNADU : District:Krishnagiri (Rural)"29,\
 "State:TAMILNADU : District:Coimbatore (Urban)"30,\
 "State:TAMILNADU : District:Tiruppur (Urban)"31,\
 "State:DELHI : District:North-West (Urban)"32,\
 "State:DELHI : District:North (Urban)"33,\
 "State:DELHI : District:North-East (Urban)"34,\
 "State:DELHI : District:East (Urban)"35,\
 "State:DELHI : District:New-Delhi (Urban)"36,\
 "State:DELHI : District:Central (Urban)"37,\
 "State:DELHI : District:West (Urban)"38,\
 "State:DELHI : District:South-West (Urban)"39,\
 "State:DELHI : District:South (Urban)"40)\
 rotate by 90 offset 0,-2.5 font "Times Italic,6"
 set ytics \
 ("State:TAMILNADU : District:Thiruvallur (Urban)"0,\
 "State:TAMILNADU : District:Chennai (Urban)"1,\
 "State:TAMILNADU : District:Kancheepuram (Urban)"2,\
 "State:TAMILNADU : District:Vellore (Rural)"3,\
 "State:TAMILNADU : District:Tiruvannamalai (Rural)"4,\
 "State:TAMILNADU : District:Viluppuram (Rural)"5,\
 "State:TAMILNADU : District:Salem (Urban)"6,\
 "State:TAMILNADU : District:Namakkal (Rural)"7,\
 "State:TAMILNADU : District:Erode (Urban)"8,\
 "State:TAMILNADU : District:The-Nilgiris (Urban)"9,\
 "State:TAMILNADU : District:Dindigul (Rural)"10,\
 "State:TAMILNADU : District:Karur (Rural)"11,\
 "State:TAMILNADU : District:Tiruchirappalli (Rural)"12,\
 "State:TAMILNADU : District:Perambalur (Rural)"13,\
 "State:TAMILNADU : District:Ariyalur (Rural)"14,\
 "State:TAMILNADU : District:Cuddalore (Rural)"15,\
 "State:TAMILNADU : District:Nagapattinam (Rural)"16,\
 "State:TAMILNADU : District:Thiruvarur (Rural)"17,\
 "State:TAMILNADU : District:Thanjavur (Rural)"18,\
 "State:TAMILNADU : District:Pudukkottai (Rural)"19,\
 "State:TAMILNADU : District:Sivaganga (Rural)"20,\
 "State:TAMILNADU : District:Madurai (Urban)"21,\
 "State:TAMILNADU : District:Theni (Urban)"22,\
 "State:TAMILNADU : District:Virudhunagar (Urban)"23,\
 "State:TAMILNADU : District:Ramanathapuram (Rural)"24,\
 "State:TAMILNADU : District:Thoothukkudi (Urban)"25,\
 "State:TAMILNADU : District:Tirunelveli (Rural)"26,\
 "State:TAMILNADU : District:Kanniyakumari (Urban)"27,\
 "State:TAMILNADU : District:Dharmapuri (Rural)"28,\
 "State:TAMILNADU : District:Krishnagiri (Rural)"29,\
 "State:TAMILNADU : District:Coimbatore (Urban)"30,\
 "State:TAMILNADU : District:Tiruppur (Urban)"31,\
 "State:DELHI : District:North-West (Urban)"32,\
 "State:DELHI : District:North (Urban)"33,\
 "State:DELHI : District:North-East (Urban)"34,\
 "State:DELHI : District:East (Urban)"35,\
 "State:DELHI : District:New-Delhi (Urban)"36,\
 "State:DELHI : District:Central (Urban)"37,\
 "State:DELHI : District:West (Urban)"38,\
 "State:DELHI : District:South-West (Urban)"39,\
 "State:DELHI : District:South (Urban)"40)\
 font "Times Italic,6"
 set title "Migration gravity algorithm, the force"
 pl "stored_data/Migration_matrix/Migration.txt" matrix w image
